FROM java:8
FROM maven:alpine

WORKDIR /deployments
COPY . /deployments
RUN mvn clean install
RUN mkdir -p /logs

EXPOSE 8080
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=$ENV","./target/spring-boot-data-jpa-example-0.0.1-SNAPSHOT.jar"]


