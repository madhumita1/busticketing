package com.test.busService.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by madhumita.g on 10/12/18.
 */

@ControllerAdvice
public class ExceptionHandlers extends ResponseEntityExceptionHandler
{

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlers.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public Error handleExceptionTopLevel(HttpServletRequest req, Exception ex)
    {

        LOGGER.error("raised on", ex.getStackTrace());

        return new Error("something went wrong");
    }

    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public Error handleException(HttpServletRequest req, IllegalStateException ex)
    {

        LOGGER.error("raised on", ex.getStackTrace());

        return new Error("something went wrong");
    }

    @ExceptionHandler(BusFullException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public Error handleBadRequestException(HttpServletRequest req,
                                                            BusFullException ex)
    {

        LOGGER.error("bad req, raised", ex);

        return new Error("Bus Capacity is full");
    }

    @ExceptionHandler(TicketNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public Error handleTicketNotFoundException(HttpServletRequest req,
                                               BusFullException ex) {

        LOGGER.error("not found, raised", ex);

        return new Error("ticket id does not exist");
    }
}