package com.test.busService.entity;

/**
 * Created by madhumita.g on 9/12/18.
 */
public enum Status
{
    OPEN,
    CLOSE
}
