package com.test.busService.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.IOException;
import java.util.Map;

@Entity
@Table(name = "ticket", schema = "spring_app_db")
@Component
@JsonIgnoreProperties(value = "id", allowGetters=true)
public class Ticket {

    @Transient
    @Ignore
    ObjectMapper objectMapper = new ObjectMapper();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonBackReference
    private Bus bus;

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    private String status = Status.OPEN.toString();

    private String userDetails;

    public Long getId()
    {
        return id;
    }

    public Status getStatus()
    {
        return Status.valueOf(status);
    }

    public void setStatus(Status status)
    {
        this.status = status.toString();
    }

    public Map<String, Object> getUserDetails()
    {
        try
        {
            return objectMapper.readValue(this.userDetails, new TypeReference<Map<String, Object>>(){});
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }
    }

    public void setUserDetails(Map<String, Object> user)
    {
        try
        {
            this.userDetails = objectMapper.writeValueAsString(user);
        }
        catch (JsonProcessingException e)
        {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String toString()
    {
        return "Ticket{" +
                "id=" + id +
                ", bus=" + bus +
                ", status='" + status + '\'' +
                ", userDetails='" + userDetails + '\'' +
                '}';
    }
}
