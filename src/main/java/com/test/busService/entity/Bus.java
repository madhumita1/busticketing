package com.test.busService.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

/**
 * Created by madhumita.g on 9/12/18.
 */
@Component
@Entity
@Table(name = "bus", schema = "spring_app_db")
@JsonIgnoreProperties(value = "tickets", allowGetters=true)
public class Bus
{
    @Transient
    @Ignore
    ObjectMapper objectMapper = new ObjectMapper();

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int totalSeats;

    @OneToMany
    @JsonManagedReference
    List<Ticket> tickets;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public int getTotalSeats()
    {
        return totalSeats;
    }

    public void setTotalSeats(int totalSeats)
    {
        this.totalSeats = totalSeats;
    }

    public List<Ticket> getTickets()
    {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets)
    {
        this.tickets = tickets;
    }

    @Override
    public String toString()
    {
        return "Bus{" +
                "id=" + id +
                ", totalSeats=" + totalSeats +
                ", tickets=" + tickets +
                '}';
    }
}
