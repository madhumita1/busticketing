package com.test.busService.repository;

import com.test.busService.entity.Bus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface BusRepository extends JpaRepository<Bus, Long>
{
}
