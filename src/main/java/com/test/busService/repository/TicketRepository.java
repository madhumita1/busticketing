package com.test.busService.repository;

import com.test.busService.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Component
public interface TicketRepository extends JpaRepository<Ticket, Long>
{
    List<Ticket> findByStatus(String status);

    List<Ticket> findByBusId(Long busId);
}
