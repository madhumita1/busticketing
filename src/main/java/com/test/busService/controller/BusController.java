package com.test.busService.controller;

import com.test.busService.entity.Bus;
import com.test.busService.repository.BusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by madhumita.g on 10/12/18.
 */

@RestController
public class BusController
{
    @Autowired
    private BusRepository busRepository;

    private static final Logger LOGGER =  LoggerFactory.getLogger(BusController.class);

    @RequestMapping(value = "/bus", produces="application/json", method = RequestMethod.POST)
    public Bus create(@RequestBody Bus bus)
    {
        LOGGER.info("bus : {}", bus.toString());
        return busRepository.save(bus);
    }

    @RequestMapping(value = "/bus/{id}", produces="application/json", method = RequestMethod.GET)
    public Bus getBus(@PathVariable(value = "id") Long id)
    {
        return busRepository.findOne(id);
    }

    @RequestMapping(value = "/buses", produces="application/json", method = RequestMethod.GET)
    public List<Bus> getBus()
    {
        return busRepository.findAll();
    }
}
