package com.test.busService.controller;

import com.test.busService.entity.Bus;
import com.test.busService.entity.Status;
import com.test.busService.entity.Ticket;
import com.test.busService.exceptions.BusFullException;
import com.test.busService.exceptions.TicketNotFoundException;
import com.test.busService.repository.BusRepository;
import com.test.busService.repository.TicketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by madhumita.g on 9/12/18.
 */

@RestController
public class TicketController
{
    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private BusRepository busRepository;

    private static final Logger LOGGER =  LoggerFactory.getLogger(TicketController.class);

    @RequestMapping(value = "/bus/{busId}/ticket", produces="application/json", method = RequestMethod.POST)
    public Ticket create(@PathVariable(value ="busId") Long busId, @RequestBody Ticket ticket)
            throws BusFullException, TicketNotFoundException
    {
        LOGGER.info("ticket : {}", ticket.toString());

        Bus bus = busRepository.findOne(busId);

        if (bus == null)
             throw new TicketNotFoundException("bus not found");

        List<Ticket> openTickets = new ArrayList<>();

        for(Ticket t : ticketRepository.findByBusId(busId)) {
            if(ticket.getStatus().toString().equalsIgnoreCase(Status.OPEN.toString()))
                openTickets.add(t);
        }

        if(openTickets == null || openTickets.size() <  bus.getTotalSeats())
        {
            ticket.setBus(bus);
            return ticketRepository.save(ticket);
        }

        throw new BusFullException();

    }

    @RequestMapping(value="/bus/ticket/{id}", method = RequestMethod.GET, produces="application/json")
    public Ticket getTicket(@PathVariable("id") Long id) throws TicketNotFoundException
    {
        Ticket ticket = ticketRepository.findOne(id);
        if(ticket == null)
            throw new TicketNotFoundException("ticket not found");
        else
            return ticket;
    }

    @RequestMapping(value="/bus/{busId}/tickets", method = RequestMethod.GET, produces="application/json")
    public List<Ticket> getTickets(@PathVariable(value = "busId") Long busId, @RequestParam(value = "status", required = false) String status)
            throws TicketNotFoundException
    {
        if(status == null) {
            return ticketRepository.findByBusId(busId);
        }
        else
        {
            if(busRepository.findOne(busId) == null)
                throw new TicketNotFoundException("bus not found");
            List<Ticket> tickets = ticketRepository.findByBusId(busId);
            List<Ticket> response = new ArrayList<>();
            for(Ticket ticket: tickets) {
                if(ticket.getStatus().toString().equalsIgnoreCase(status)) {
                    response.add(ticket);
                }
            }
            return response;
        }
    }

    @RequestMapping(value = "/bus/ticket/{id}", produces="application/json", method = RequestMethod.PATCH)
    public Ticket update(@PathVariable("id") Long ticketId, @RequestBody Map<String, Object> valuesToBeModified)
            throws TicketNotFoundException
    {
        Ticket ticket = ticketRepository.findOne(ticketId);
        if(ticket == null)
            throw new TicketNotFoundException("ticket not found");

        if(valuesToBeModified.containsKey("userDetails")) {
            ticket.setUserDetails((Map<String, Object>) valuesToBeModified.get("userDetails"));
        }
        if(valuesToBeModified.containsKey("status")) {
            ticket.setStatus(Status.valueOf((String) valuesToBeModified.get("status")));
        }
        return ticketRepository.save(ticket);
    }
}
