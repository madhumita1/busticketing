package com.test.busService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.busService.controller.TicketController;
import com.test.busService.entity.Bus;
import com.test.busService.entity.Status;
import com.test.busService.entity.Ticket;
import com.test.busService.repository.BusRepository;
import com.test.busService.repository.TicketRepository;
import com.test.busService.util.IntegrationTestUtil;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TicketController.class)
public class TicketControllerTests
{


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TicketRepository repo;

    @MockBean
    private BusRepository busRepo;

    private Bus bus;

    @Before
    public void setup() {
        bus = new Bus();
        bus.setId(Long.parseLong("1"));
        bus.setTotalSeats(40);
        bus.setTickets(new ArrayList<>());
    }

    @Ignore
    @Test
    public void createTickets_whenAddTicket_shouldReturnSuccess() throws Exception {
        Ticket ticket = new Ticket();
        ticket.setBus(bus);
        HashMap<String, Object> userDetails = new HashMap<>();
        userDetails.put("name", "a");

        ticket.setUserDetails(userDetails);
        when(repo.findByBusId(bus.getId())).thenReturn(new ArrayList<Ticket>());
        when(busRepo.findOne(bus.getId())).thenReturn(bus);
        when(repo.save(ticket)).thenReturn(ticket);

        this.mockMvc.perform(post("/bus/1/ticket")
                                     .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                                     .content(IntegrationTestUtil.convertObjectToJsonBytes(ticket)))
                .andExpect(status().is2xxSuccessful());
    }

    @Ignore
    @Test
    public void createTickets_whenAddTicketCountGreaterThanLimit_shouldReturnError() throws Exception
    {
        List<Ticket> ticketList = new LinkedList<>();
        for (int i = 0; i < bus.getTotalSeats(); i++)
        {
            Ticket ticket = new Ticket();
            ticket.setBus(bus);

            HashMap<String, Object> userDetails = new HashMap<>();
            userDetails.put("name", "a");

            ticket.setUserDetails(userDetails);

            ticketList.add(ticket);
        }
        when(repo.findByBusId(bus.getId())).thenReturn(ticketList);
        when(busRepo.findOne(ticketList.get(0).getBus().getId())).thenReturn(bus);
        when(repo.save(ticketList.get(0))).thenReturn(ticketList.get(0));

        this.mockMvc.perform(post("/bus/1/ticket")
                                     .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                                     .content(IntegrationTestUtil.convertObjectToJsonBytes(ticketList.get(0))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getTickets_whenStatusParamIsCLOSE_shouldreturnCLOSEDTickets() throws Exception
    {
        List<Ticket> ticketList = new LinkedList<>();

        List<Ticket> expectedResults = new LinkedList<>();
        for (int i = 0; i < bus.getTotalSeats(); i++)
        {
            Ticket ticket = new Ticket();

            HashMap<String, Object> userDetails = new HashMap<>();
            userDetails.put("name", "a");

            ticket.setUserDetails(userDetails);
            if (i > 10)
            {
                ticket.setStatus(Status.CLOSE);
                expectedResults.add(ticket);
            }
            ticket.setBus(bus);
            ticketList.add(ticket);
        }
        bus.setTickets(ticketList);
        when(busRepo.findOne(bus.getId())).thenReturn(bus);
        when(repo.findByBusId(bus.getId())).thenReturn(ticketList);

        this.mockMvc.perform(get("/bus/1/tickets?status=CLOSE"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json((new ObjectMapper()).writeValueAsString(expectedResults)));

    }

    @Test
    public void getTickets_whenStatusParamIsEmpty_shouldreturnAllTickets() throws Exception
    {
        List<Ticket> ticketList = new LinkedList<>();
        for (int i = 0; i < bus.getTotalSeats(); i++)
        {
            Ticket ticket = new Ticket();

            HashMap<String, Object> userDetails = new HashMap<>();
            userDetails.put("name", "a");

            ticket.setUserDetails(userDetails);
            ticket.setBus(bus);
            ticket.setStatus(Status.CLOSE);
            ticketList.add(ticket);

        }
        bus.setTickets(ticketList);
        when(busRepo.findOne(bus.getId())).thenReturn(bus);
        when(repo.findByBusId(bus.getId())).thenReturn(ticketList);

        this.mockMvc.perform(get("/bus/1/tickets"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json((new ObjectMapper()).writeValueAsString(ticketList)));

    }

    @Test
    public void getTickets_whenStatusParamIsOPEN_shouldreturnOPENTickets() throws Exception
    {
        List<Ticket> ticketList = new LinkedList<>();

        List<Ticket> expectedResults = new LinkedList<>();
        for (int i = 0; i < bus.getTotalSeats(); i++)
        {

            Ticket ticket = new Ticket();
            ticket.setBus(bus);
            HashMap<String, Object> userDetails = new HashMap<>();
            userDetails.put("name", "a");
            ticket.setUserDetails(userDetails);
            if(i <= 10) {
                expectedResults.add(ticket);
            }
            if (i > 10)
            {
                ticket.setStatus(Status.CLOSE);
            }

            ticketList.add(ticket);
        }
        bus.setTickets(ticketList);
        when(repo.findByBusId(bus.getId())).thenReturn(ticketList);
        when(busRepo.findOne(bus.getId())).thenReturn(bus);
        this.mockMvc.perform(get("/bus/1/tickets?status=OPEN"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json((new ObjectMapper()).writeValueAsString(expectedResults)));
    }

    @Test
    public void patch_whenOnlyUserDetailsIsUpdated_shouldreturnWithUpdatesUserDetails() throws Exception
    {
        Ticket ticket = new Ticket();
        HashMap<String, Object> userDetails = new HashMap<>();
        HashMap<String, Object> params = new HashMap<>();

        userDetails.put("name", "a");
        params.put("userDetails", userDetails);
        ticket.setUserDetails(userDetails);

        Map<String, Object> updatedUserDetails = new HashMap<>();
        userDetails.put("name", "b");

        when(repo.findOne(Long.parseLong("1"))).thenReturn(ticket);

        ticket.setUserDetails(updatedUserDetails);

        when(repo.save(ticket)).thenReturn(ticket);


        this.mockMvc.perform(patch("/bus/ticket/1").contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(params)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json((new ObjectMapper()).writeValueAsString(ticket)));
    }

    @Test
    public void patch_whenOnlyStatusIsUpdated_shouldreturnWithUpdatesStatus() throws Exception
    {
        Ticket ticket = new Ticket();
        HashMap<String, Object> userDetails = new HashMap<>();
        HashMap<String, Object> params = new HashMap<>();

        params.put("userDetails", userDetails);
        params.put("status", Status.CLOSE.toString());

        userDetails.put("name", "a");

        ticket.setUserDetails(userDetails);

        Map<String, Object> updatedUserDetails = new HashMap<>();
        userDetails.put("name", "b");

        when(repo.findOne(Long.parseLong("1"))).thenReturn(ticket);

        ticket.setUserDetails(updatedUserDetails);
        ticket.setStatus(Status.CLOSE);

        when(repo.save(ticket)).thenReturn(ticket);

        ticket.setStatus(Status.CLOSE);

        when(repo.save(ticket)).thenReturn(ticket);

        this.mockMvc.perform(patch("/bus/ticket/1").contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                                     .content(IntegrationTestUtil.convertObjectToJsonBytes(params)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json((new ObjectMapper()).writeValueAsString(ticket)));
    }

    @Test
    public void patch_whenBothAreUpdated_shouldreturnWithBothFieldsUpdated() throws Exception
    {
        Ticket ticket = new Ticket();
        HashMap<String, Object> userDetails = new HashMap<>();
        HashMap<String, Object> params = new HashMap<>();
        params.put("status", Status.CLOSE.toString());

        userDetails.put("name", "a");
        ticket.setUserDetails(userDetails);


        when(repo.findOne(Long.parseLong("1"))).thenReturn(ticket);

        ticket.setStatus(Status.CLOSE);

        when(repo.save(ticket)).thenReturn(ticket);

        this.mockMvc.perform(patch("/bus/ticket/1").contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                                     .content(IntegrationTestUtil.convertObjectToJsonBytes(params)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json((new ObjectMapper()).writeValueAsString(ticket)));
    }
}