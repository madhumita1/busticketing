
package com.test.busService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.busService.controller.BusController;
import com.test.busService.entity.Bus;
import com.test.busService.repository.BusRepository;
import com.test.busService.util.IntegrationTestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BusController.class)
public class BusControllerTests
{


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BusRepository busRepository;

    @Test
    public void createBus_whenAddBus_shouldReturnSuccess() throws Exception
    {
        Bus bus = new Bus();
        bus.setId(Long.parseLong("1"));
        bus.setTotalSeats(10);

        when(busRepository.save(bus)).thenReturn(bus);

        this.mockMvc.perform(post("/bus")
                                     .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                                     .content(IntegrationTestUtil.convertObjectToJsonBytes(bus)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void getBus_whenSingleBus_shouldReturnSuccess() throws Exception
    {
        Bus bus = new Bus();
        bus.setId(Long.parseLong("1"));
        bus.setTotalSeats(10);

        when(busRepository.findOne(Long.parseLong("1"))).thenReturn(bus);

        this.mockMvc.perform(get("/bus/1"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json((new ObjectMapper()).writeValueAsString(bus)));

    }

    @Test
    public void getBuses_when2BusesAdded_shouldReturn2Buses() throws Exception
    {
        List<Bus> busList = new ArrayList<>();
        for (Integer i =1 ; i <= 2; i++){
            Bus bus = new Bus();
            bus.setId(Long.parseLong(i.toString()));
            bus.setTotalSeats(10);
            busList.add(bus);
        }

        when(busRepository.findAll()).thenReturn(busList);

        this.mockMvc.perform(get("/buses"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json((new ObjectMapper()).writeValueAsString(busList)));

    }



}

