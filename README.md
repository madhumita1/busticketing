I have used Java spring boot, hibernate ORM  with postgres DB. As these libraries are highly optimized and reliable, I have written an abstracted code and only functional tests to cover the controller level code.
There are two entities - 
Bus
Tickets
Bus to ticket has a one to many relationship.

the .gitlab-ci has the configuration for running the tests when any commit is made, and merge is done only when the build is successful

If the build is successful, it will create a docker image and push it to the gitlab registry

image can be pulled from here and run on the staging or deployment server

There are two separate docker-compose files for dev and staging environment, should be replicated for production env with the necessary changes

**To build the docker image on local, run the command**


 docker build -t spring-boot-jpa-image .

**to run the container**

docker-compose up

**To try the APIs, open the following url**

_url:8087://swagger-ui.html

Logs are in the /deployment/logs folder inside the container. To persist them on your system, create a /logs folder in the root of the project

POSTMAN COLLECTION : https://www.getpostman.com/collections/0963f6f9fe4b5f730c87
