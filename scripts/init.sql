CREATE USER docker;
CREATE DATABASE spring_app_db;
GRANT ALL PRIVILEGES ON DATABASE spring_app_db TO postgres;
use spring_app_db;

CREATE TABLE user()